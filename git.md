## commit

* **git commit** - tworzy commita z aktualnie zmienionych plików
* **git commit -m "wiadomosc"** - tworzy commmita z podaną w cudzysłowach wiadomością
* **git commit --amend -m "{wiadomość}"** - umożliwia zmianę ostatniego commita
* **git commit --amend -m "dsfsdf"** - modyfikuje komentarz ostatniego commita
* **git commit --date="2017-08-18T13:23:41" -m ""** - comit ze wskazaną datą
* **git commit -n** - pomija git hooks
* **git revert {numer commita}** - tworzy nowego commita z cofnięciem zmian ze wskazanego commita
* **git amend** - zmienia poprzedniego commita
## log

* **git log** - wyświetla listę commitów (od najnowszego)
* **git log -{numer}** wyświetla podaną liczbę ostatnich commitów
* **git log --oneline** - wyświetla commity w postaci skróconej
* **git log -{numer} --oneline** wyświetla podaną liczbę ostatnich commitów w postaci skróconej
* **git log --graph --decorate --oneline** - pokazuje graficzny obraz zmian
* **git log --author={nazwa użytkownika}** - pokazuje commity danego użytkownika
* **git shortlog** - lista commitów użytkowników
* **git shortlog -s -n** - lista użytkowników repozytorium
* **git log --author={nazwa}** - comity danego autora
* **git llog --after=2016-08-16 --before=2016-08-30** - commity z podanego zakresu (--until starsze od podanej daty, --since z pred podanej daty)
* **git log --name-status** - podaje statuz zmian przy nazwie pliku (add, mod, delete)
* **git log --stat** - + statystyki zmian w plikach (--shortstat bez ++++---)

